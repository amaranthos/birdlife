﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(CanvasGroup))]
public class Menu : MonoBehaviour {
	private Animator anim;
	private CanvasGroup group;

	public bool IsOpen {
		get { return anim.GetBool("IsOpen"); }
		set { anim.SetBool("IsOpen", value); }
	}

	private void Awake() {
		anim = GetComponent<Animator>();
		group = GetComponent<CanvasGroup>();

		var rect = GetComponent<RectTransform>();
		rect.offsetMax = rect.offsetMin = new Vector2(0, 0);
	}

	public void Update() {
		if (!anim.GetCurrentAnimatorStateInfo(0).IsName("Open")) {
			group.blocksRaycasts = group.interactable = false;
		}
		else {
			group.blocksRaycasts = group.interactable = true;
		}
	}
}
