﻿using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.Collections;
using System.Collections.Generic;

public class DebugPanel : MonoBehaviour {
	static DebugPanel main;
	public Text panel;
	public Dictionary<string, StringBuilder> textGroups;
	public string currentGroup;

	void Awake(){
		main = this;
		textGroups = new Dictionary<string, StringBuilder>();
	}

	void Update(){
		panel.text = "";
		var buf = new StringBuilder();

		foreach(var kv in textGroups){
			buf.Append("<b>" + kv.Key + "</b>\n");
			buf.Append(kv.Value + "\n");
		}

		panel.text = buf.ToString();
	}

	static public void BindGroup(string _g){
		if(!main || !main.gameObject.activeInHierarchy) return;
		
		main.currentGroup = _g;
		if(!main.textGroups.ContainsKey(_g)) main.textGroups[_g] = new StringBuilder();
	}

	static public void Clear(string group = null){
		if(!main || !main.gameObject.activeInHierarchy) return;

		if(group == null){
			main.textGroups[main.currentGroup].Length = 0;
		}else{
			if(!main.textGroups.ContainsKey(group)) main.textGroups[group] = new StringBuilder();
			main.textGroups[group].Length = 0;
		}
	}

	static public void Break(string group = null){
		if(!main || !main.gameObject.activeInHierarchy) return;
		
		if(group == null){
			main.textGroups[main.currentGroup].AppendLine();
		}else{
			if(!main.textGroups.ContainsKey(group)) main.textGroups[group] = new StringBuilder();
			main.textGroups[group].AppendLine();
		}
	}

	static public void Append(params object[] os){
		if(!main || !main.gameObject.activeInHierarchy) return;
		
		foreach(var o in os){
			main.textGroups[main.currentGroup].Append(o.ToString());
		}

		main.textGroups[main.currentGroup].AppendLine();
	}

	static public void AppendGroup(string group, params object[] os){
		if(!main || !main.gameObject.activeInHierarchy) return;
		var pg = main.currentGroup;
		
		BindGroup(group);
		Append(os);

		main.currentGroup = pg;
	}

	static public void SetGroup(string group, params object[] os){
		if(!main || !main.gameObject.activeInHierarchy) return;
		var pg = main.currentGroup;

		BindGroup(group);
		Clear(group);
		Append(os);

		main.currentGroup = pg;
	}
}
