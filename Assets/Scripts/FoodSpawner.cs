﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class SpawnArea {
	public Vector3 position;
	public float radius;
	
	public SpawnArea(Vector3 p, float r = 1f){
		position = p;
		radius = r;
	}
}

public class FoodSpawner : MonoBehaviour {
	public GameObject foodPrefab; // Expand to array later
	public int maxFood = 2;
	public float foodSpawnTimeout = 5f;

	public List<SpawnArea> spawnAreas;
	private List<GameObject> food = new List<GameObject>();
	private float spawnTimer = 0f;

	void Update(){
		food.RemoveAll(x => x == null);

		if(food.Count < maxFood){
			spawnTimer -= Time.deltaTime;

			if(spawnTimer < 0f){
				SpawnFood();
				spawnTimer = foodSpawnTimeout;
			}
		}
	}

	void SpawnFood(){
		if(spawnAreas.Count == 0) {
			Debug.LogError("No food spawn areas");
			return;
		}

		if(foodPrefab == null){
			Debug.LogError("Food spawner prefab not set");
			return;			
		}

		var area = spawnAreas[Random.Range(0, spawnAreas.Count)];
		var pointInArea = Random.insideUnitCircle * area.radius;

		var checkDist = 5f;
		var spawnPos = area.position + new Vector3(pointInArea.x, 0f, pointInArea.y) + Vector3.up*(checkDist*0.5f);

		RaycastHit hit;
		if(Physics.Raycast(spawnPos, -Vector3.up, out hit, checkDist)){
			var go = Instantiate(foodPrefab, hit.point, Quaternion.identity) as GameObject;
			go.transform.parent = transform;
			food.Add(go);
		}else{
			Debug.LogError("Food raycast failed");
		}
	}
}
