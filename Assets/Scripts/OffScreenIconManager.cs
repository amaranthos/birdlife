﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class OffScreenIcon {
	public float lifetime; 
	public GameObject gameObject;
	public SpriteRenderer renderer;
	public Vector3 worldPosition;
	public bool livesForever = false;
	public float depth = 0f;

	public float prominence = 0f;

	public void SetIcon(ImageKey icon){
		var tex = ChirpImageManager.main.GetImage(icon);
		renderer.sprite = Sprite.Create(tex, new Rect(0,0,tex.width,tex.height), new Vector2(0.5f, 0.5f), tex.width/2f);
	}
}

public class OffScreenIconManager : MonoBehaviour {
	public static OffScreenIconManager main;

	public float iconLifeTime = 3f;
	public Camera cam;
	public GameObject iconPrefab;

	private List<OffScreenIcon> icons = new List<OffScreenIcon>();

	void Awake(){
		main = this;
	}

	void Update(){
		foreach(var i in icons){
			UpdateIcon(i);

			var c = i.renderer.material.color;

			if(!i.livesForever){
				c.a = 1f - i.lifetime/iconLifeTime;
				c.a = 1f - c.a*c.a;
				c.a = i.prominence * c.a;
				
				i.lifetime -= Time.deltaTime;
				if(i.lifetime < 0f) {
					Destroy(i.gameObject);
					i.gameObject = null;
				}
			}else{
				c.a = i.prominence;
			}

			i.renderer.material.color = c;
		}

		icons.RemoveAll(i => (i.gameObject == null || i.renderer == null));
	}

	void UpdateIcon(OffScreenIcon i){
		var ct = cam.transform;
		var pt = Camera.main.transform;
		var diff = i.worldPosition - pt.position;
		var dir = diff.normalized;

		var rightness = Vector3.Dot(pt.right, dir);
		var upness = Vector3.Dot(pt.up, dir);
		var backness = Vector3.Dot(-pt.forward, dir);

		var spos = ct.position + ct.forward*(1.04f + i.depth*0.1f) + (ct.right * rightness * cam.aspect + ct.up * (upness - backness)*0.8f) * cam.orthographicSize * 0.8f;

		i.gameObject.transform.position = spos;

		i.prominence = (backness + 0.6f)/2f;
		i.prominence += Mathf.Clamp(diff.magnitude-10f, 0f, 50f)/50f;
		i.prominence = Mathf.Clamp(i.prominence/2f, 0f, 1f);
	}

	public OffScreenIcon NewIcon(ImageKey type, Vector3 pos, bool livesForever = false){
		var ct = cam.transform;

		var oi = new OffScreenIcon();
		oi.lifetime = livesForever?Mathf.Infinity:iconLifeTime;
		oi.livesForever = livesForever;
		oi.gameObject = Instantiate(iconPrefab, ct.position - ct.forward, Quaternion.identity) as GameObject;
		oi.gameObject.transform.parent = ct;

		// var tex = ChirpImageManager.main.GetImage(type);
		oi.renderer = oi.gameObject.GetComponent<SpriteRenderer>();
		oi.SetIcon(type);

		oi.worldPosition = pos;

		icons.Add(oi);

		return oi;
	}

	public void Clear(){
		foreach(var i in icons){
			Destroy(i.gameObject);
		}

		icons.Clear();
	}
}
