﻿using UnityEngine;
using System.Collections;

public class DestroyAfterTime : MonoBehaviour {
	public float lifetime = 1f;

	IEnumerator Start () {
		yield return new WaitForSeconds(lifetime);
		Destroy(gameObject);
	}
}