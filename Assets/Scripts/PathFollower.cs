﻿using UnityEngine;
using System.Collections;

public class PathFollower : MonoBehaviour {
	public Path path;
	public float leeway = 1f;
	public int index = 0;

	void Start(){
		index = path.GetNearestIndex(transform.position);
	}

	public Vector3 GetCurrent(){
		return path.GetWaypoint(index);
	}

	public Vector3 Next(){
		index++;
		index %= path.waypoints.Count;

		return path.GetWaypoint(index);
	}

	public bool HasReachedPoint(){
		return Vector3.Distance(GetCurrent(), transform.position) < leeway;
	}
}
