﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChirpImageManager : MonoBehaviour {
	static public ChirpImageManager main;

	[System.Serializable]
	public class ImageItem {
		public ImageKey key;
		public Texture2D image;
	}

	public List<ImageItem> images = new List<ImageItem>();

	private void Awake() {
		main = this;
	}

	public Texture2D GetImage(ImageKey image) {
		if(image == ImageKey.None) return null;
		return images.Find(item=>item.key==image).image;
	}
}

public enum ImageKey {
	None,
	Anger,
	Follow,
	Wait,
	Food,
	Happy,
	Sad,
	Death,
	Locate,
}
