﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class TreeColouriser : MonoBehaviour {
	public Shader shader;
	public Gradient gradient;
	public Color color;
	
	public bool lockColor = false;

	Material mat;
	ParticleSystem partSys;

	void Update () {
		SetColor(color);
	}

	void SetColor(Color c){
		if(partSys) partSys.startColor = c;
		if(mat) mat.SetColor("_Color", c);
	}

	public void ReRandomiseColour(){
		if(!lockColor){
			color = gradient.Evaluate(Random.value);
			SetColor(color);
		}
	}

	public void ApplyMaterials(){
		if(!mat){
			mat = new Material(shader);
		}
		if(!partSys){
			partSys = GetComponentInChildren<ParticleSystem>();
		}

		var crenderers = GetComponentsInChildren<Renderer>();
		foreach(var r in crenderers){
			if(r.CompareTag("Foliage")){
				r.material = mat;
			}
		}
	}
}
