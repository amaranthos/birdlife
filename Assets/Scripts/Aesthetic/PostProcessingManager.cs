﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class PostProcessingManager : MonoBehaviour {
	static public PostProcessingManager main;
	public Material postProcessMaterial;

	float desaturation = 0f;
	float targetDesaturation = 0f;

	float vignette = 0f;
	float targetVignette = 0f;

	float whiteOut = 0f;
	float targetWhiteOut = 0f;

	public float rainParam = 0f;
	public float playerHungerParam = 0f;
	public bool playerDeath = false;
	public bool chickDeath = false;
	public bool chickSurvive = false;
	public bool playerLeave = false;

	void Awake(){
		main = this;
		SetBlackOut(false);
	}

	void OnDestroy(){
		SetBlackOut(false);
	}

	void Update(){
		CalculateTargets();
		SetBlackOut(playerDeath);

		desaturation = Mathf.Lerp(desaturation, targetDesaturation, Time.deltaTime);
		vignette = Mathf.Lerp(vignette, targetVignette, Time.deltaTime);
		whiteOut = Mathf.Lerp(whiteOut, targetWhiteOut, Time.deltaTime*0.4f);

		postProcessMaterial.SetFloat("desaturateAmt", desaturation);
		postProcessMaterial.SetFloat("vignetteAmt", vignette);
		postProcessMaterial.SetFloat("whiteOutAmt", whiteOut);
	}

	void CalculateTargets(){
		var transformedPH = playerHungerParam;
		transformedPH = transformedPH*transformedPH;

		DebugPanel.BindGroup("PostProcessingManager");
		DebugPanel.Clear();
		DebugPanel.Append(transformedPH);

		targetDesaturation = rainParam*0.4f + transformedPH;
		targetVignette = rainParam*0.3f + 0.4f + transformedPH*0.3f;

		if(chickDeath){
			targetDesaturation = Mathf.Max(targetDesaturation, 0.8f);
			targetVignette = Mathf.Max(targetVignette, 0.8f);
		}

		targetDesaturation = Mathf.Clamp(targetDesaturation, 0f, 1f);
		targetVignette = Mathf.Clamp(targetVignette, 0f, 1f);

		if(chickSurvive){
			targetDesaturation = -0.5f;
			targetVignette = 0.2f;
		}

		if(playerLeave){
			targetWhiteOut = 1f;
		}
	}

	void OnRenderImage (RenderTexture source, RenderTexture destination){
		Graphics.Blit(source, destination, postProcessMaterial);
	}

	public void SetBlackOut(bool blackOut){
		postProcessMaterial.SetInt("blackOut", blackOut?1:0);
	}

	public void SetDesaturation(float des){
		targetDesaturation = des;
	}
	public void SetVignette(float vig){
		targetVignette = vig;
	}
	public void SetWhiteOut(float wo){
		targetWhiteOut = wo;
	}
	public void SetWhiteOutHard(float wo){
		whiteOut = wo;
		postProcessMaterial.SetFloat("whiteOutAmt", whiteOut);
	}
}
