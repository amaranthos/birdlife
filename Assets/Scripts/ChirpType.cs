public enum ChirpType {
	Command,
	Acknowledge,
	Hungry,
	Happy,
	Sad,
	Death,

	// Crow
	Angry,
}