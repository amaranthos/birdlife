﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour {
	public float cameraAngle = 30f;
	public float cameraDist = 2f;

	public float maxUseRadius = 1f;
	public float altitudeFunction = 0.012f;

	public float chirpVol = 0.7f;

	public Transform mouthPos;

	public float groundSpeed = 3f;
	public AnimationCurve hungerSlowDown;

	public float hungerFlightThreshold = 0.3f;
	public float maxFlapForce = 0.5f;
	public float flapPower = 1f;
	public float hungerPeriod = 1f; // Days till starve
	public float hungerChirpThreshold = 0.65f;
	public Vector2 hungerChirpPeriod = new Vector2(10f, 30f);

	public float hunger = 1f;
	private float hungerChirpTimer = 0f;

	private Vector3 camfwd = Vector3.zero;
	private float cameraControlTimer = 0f;
	private Camera cam;
	private Transform t;
	private Rigidbody r;
	private Animator a;
	private ParticleSystem tweetEmitter;
	private ParticleSystem chirpEmitter;

	private GameObject carriedItem;

	public float chirpCooldown = 0.5f;
	private float chirpTimer = 0.0f;

	public float flapCooldown = 0.1f;
	private float flapTimer = 0.0f;

	private float fogBank = 0f;
	public bool isGrounded = false;

	public SpriteRenderer aboveHeadIcon;

	public SpriteRenderer useCtxBtn;
	private bool nearFood = false;
	private float useCtxBtnFadeAmt = 0f;

	void Awake(){
		cam = Camera.main;

		t = transform;
		r = GetComponent<Rigidbody>();
		a = GetComponentInChildren<Animator>();

		var pems = GetComponentsInChildren<ParticleSystem>();

		foreach(var pem in pems){
			switch(pem.name){
				case "TweetEmitter":
					tweetEmitter = pem;
					break;
				case "ChirpEmitter":
					chirpEmitter = pem;
					break;
			}
		}
	}

	void Start(){
		GameManager.main.player = this;
		camfwd = cam.transform.forward;
		camfwd = Vector3.RotateTowards(camfwd, t.up, cameraAngle*3.1415926f/180f, 0f);
	}

	void Update(){
		cameraControlTimer += Time.deltaTime;
		chirpTimer += Time.deltaTime;
		flapTimer += Time.deltaTime;

		UpdateAboveHeadIcon();

		if(!GameManager.main.GameCanEnd())
			hunger -= Time.deltaTime / (hungerPeriod * EnvironmentManager.main.dayLength);
		// if(hunger < 0f) GameManager.main.OnPlayerDeath("You starved");

		hunger = Mathf.Clamp(hunger, 0f, 1f);

		PostProcessingManager.main.playerHungerParam = 1f - hunger;
		
		hungerChirpTimer -= Time.deltaTime;
		if(hunger < hungerChirpThreshold && hungerChirpTimer < 0f){
			Chirp(ChirpType.Hungry);

			hungerChirpTimer = Random.Range(hungerChirpPeriod.x, hungerChirpPeriod.y);
		}

		fogBank = Mathf.Lerp(fogBank, 0f, Time.deltaTime);

		RaycastHit dummy;
		isGrounded = Physics.SphereCast(t.position, 0.05f, -Vector3.up, out dummy, t.localScale.y*0.5f);

		a.SetBool("IsFlying", !isGrounded);

		DebugPanel.BindGroup("PlayerController");
		DebugPanel.Clear();
		DebugPanel.Append("Hunger: ", hunger*100f, "%");

		if(Input.GetKeyDown("space") || Input.GetButtonDown("Fire1")){
			Flap();
		}

		if(Input.GetButtonDown("Fire2")){
			if (chirpTimer >= chirpCooldown) {
				chirpTimer = 0.0f;
				Chirp(ChirpType.Command);
			}
		}

		if(Input.GetButtonDown("Fire3")){
			TryUse();
		}

		if(isGrounded){
			UpdateWalking();
		}else{
			UpdateFlying();
		}

		UpdateCameraPosition();

		var velm = Mathf.Max(Vector3.Dot(r.velocity, t.forward), 0f);
		Camera.main.fieldOfView = Mathf.Lerp(Camera.main.fieldOfView, Mathf.Lerp(50f, 100f, velm/12f), Time.deltaTime*0.5f);

		var altitude = t.position.y / 30f;
		AudioManager.main.SetAltitude(altitude);

		// Context button while near food
		if(!nearFood) useCtxBtnFadeAmt -= Time.deltaTime*0.5f;
		else useCtxBtnFadeAmt += Time.deltaTime*0.1f;

		useCtxBtnFadeAmt = Mathf.Clamp(useCtxBtnFadeAmt, 0f, 1f);

		var ctxcol = useCtxBtn.color;
		ctxcol.a = useCtxBtnFadeAmt;
		useCtxBtn.color = ctxcol;
	}

	void UpdateWalking(){
		Pitch(0f);
		Bank(fogBank);

		var movement = Mathf.Clamp(hunger, 0f, 1f);
		movement = hungerSlowDown.Evaluate(movement) * groundSpeed;

		var vel = r.velocity;

		var nvel = t.forward * Input.GetAxis("Vertical") * movement;
		nvel.y = vel.y;

		t.rotation *= Quaternion.Euler(0f, Input.GetAxis("Horizontal") * Mathf.PI, 0f);

		vel = Vector3.Lerp(vel, nvel, 0.5f);

		r.velocity = vel;

		AudioManager.main.SetWindAmount(0f);
		a.SetFloat("WalkVel", vel.magnitude);
	}

	void UpdateFlying(){
		// Handles banking + rotation
		var velm = Mathf.Max(Vector3.Dot(r.velocity, t.forward), 0f);
		var bankMult = 1f / (velm*0.35f + 1f);

		Pitch(Input.GetAxis("Vertical") * 30f);
		Bank(-Input.GetAxis("Horizontal") * 60f * bankMult + fogBank);

		AudioManager.main.SetWindAmount(Mathf.Clamp(r.velocity.magnitude / 15f, 0f, 1f));
	}

	void UpdateCameraPosition(){
		// Make camera ignore banking
		var camt = cam.transform;
		camfwd = Vector3.Slerp(camfwd, Vector3.Scale(t.forward, new Vector3(1f, 0.6f, 1f)), Time.deltaTime*Mathf.Min(cameraControlTimer, 4f));

		var look = Vector3.RotateTowards(camfwd, -t.up, cameraAngle*3.1415926f/180f, 0f);
		var up = Vector3.RotateTowards(t.up, t.forward, cameraAngle*3.1415926f/180f, 0f);
		var npos = t.position - look*cameraDist;
		
		camt.position = npos;
		camt.rotation = Quaternion.LookRotation(look, up);
	}

	void UpdateAboveHeadIcon(){
		// Position
		var target = t.position + Vector3.up * (0.25f + Mathf.Sin(Time.time*2f)*0.02f);
		var aht = aboveHeadIcon.transform;

		aht.position = Vector3.Lerp(aht.position, target, Time.deltaTime*8f);

		// Alpha
		var ac = aboveHeadIcon.color;
		ac.a = Mathf.Lerp(ac.a, 1f - Mathf.Clamp(hunger/hungerChirpThreshold, 0f, 1f), Time.deltaTime*2f);
		aboveHeadIcon.color = ac;
	}

	const float turnMult = 60f;

	void Bank(float amt){
		var curbank = t.rotation.eulerAngles.z;
		if(curbank > 180f) curbank -= 360f;

		t.rotation *= Quaternion.Euler(0f, -amt * (Time.deltaTime * turnMult * 0f + 1f) / 20f, (amt - curbank) * Time.deltaTime *(0.03f * turnMult * 0f + 1f));
	}

	void Pitch(float angle){
		var curpitch = t.rotation.eulerAngles.x;
		if(curpitch > 180f) curpitch -= 360f;

		t.rotation *= Quaternion.Euler((angle - curpitch) * (Time.deltaTime * turnMult * 0f + 1f), 0f, 0f);
	}

	void Flap(){
		if (flapTimer < flapCooldown) return;
		flapTimer = 0.0f;

		var altitude = t.position.y * altitudeFunction;
		var force = t.up * Mathf.Clamp(1f - altitude, 0f, 1f);
		force += t.forward * 0.1f;

		var flightPower = Mathf.Clamp(hungerSlowDown.Evaluate(hunger) * flapPower, 0f, maxFlapForce);

		DebugPanel.BindGroup("PlayerControllerFlap");
		DebugPanel.Clear();
		DebugPanel.Append("Altitude: ", altitude);
		DebugPanel.Append("Flight Power: ", flightPower);

		AudioManager.main.PlaySoundEffect(SoundEffect.BirdFlap, t.position);

		r.AddForce(force * flightPower, ForceMode.Impulse); // 1N
		if(isGrounded) {
			a.SetTrigger("TakeOff");
		} else {
			a.SetTrigger("Flap");
		}
	}

	void OnCollisionEnter(Collision col){
		// Add exception for enemies + chick

		if(GameManager.main.IsGameOver()) return;

		float impactVel = Vector3.Dot(col.contacts[0].normal, col.relativeVelocity);
		float forwardVel = Vector3.Dot(t.forward, -col.contacts[0].normal);
		DebugPanel.SetGroup("PlayerCollision", impactVel, " * ", forwardVel, " ", impactVel * forwardVel);
		if(impactVel*forwardVel > 10f){
			GameManager.main.OnPlayerDeath("You died on impact");
			AudioManager.main.PlaySoundEffect(SoundEffect.BirdDie, t.position);
		}
	}

	void OnTriggerEnter(Collider col){
		if(col.CompareTag("Crow") && !isGrounded){
			var cvel = col.GetComponent<Rigidbody>().velocity;
			var vdiff = (cvel-r.velocity).magnitude;
			
			if(vdiff > 2f) col.SendMessage("Damage");
		}else if(col.CompareTag("DeathBox")){
			GameManager.main.OnPlayerDeath("You were hit by a car");
			AudioManager.main.PlaySoundEffect(SoundEffect.BirdDie, t.position);
		}
	}

	void OnTriggerStay(Collider col){
		if(col.CompareTag("Fog")){
			var dirToCenter = (new Vector3(-50f, 0f, -40f)-t.position).normalized;
			var forwardness = Vector3.Dot(t.forward, dirToCenter);

			// Sign returns 1f for 0f which is wrong but that's what we want anyway
			var rightness = Mathf.Sign(Vector3.Dot(t.right, dirToCenter)); 

			var nfogBank = Mathf.Max(-forwardness, 0f) * -75f * rightness;
			fogBank = Mathf.Lerp(fogBank, nfogBank, Time.deltaTime/5f);

			r.AddForce(dirToCenter * 0.4f);

			DebugPanel.BindGroup("FogBank");
			DebugPanel.Clear();
			DebugPanel.Append(fogBank);
		}else if(col.CompareTag("EndFog")){
			GameManager.main.OnPlayerLeave();
		}else if(col.CompareTag("FoodVol")){
			nearFood = true;
		}else if(col.CompareTag("FlapVol")){
			r.AddForce(Vector3.up*2f);

			Flap();
		}
	}

	void OnTriggerExit(Collider col){
		if(col.CompareTag("Fog")){
			fogBank = 0f;
		}else if(col.CompareTag("FoodVol")){
			nearFood = false;
		}
	}

	void TryUse() {
		nearFood = false;
		if(carriedItem) {
			var useable = carriedItem.GetComponent<IUseable>();
			useable.Use(this);

		} else {
			List<IUseable> nearby = CheckSurroundingsForUsable();

			if(nearby.Count > 0) {
				nearby[0].Use(this);
			}
		} 
	}

	List<IUseable> CheckSurroundingsForUsable() {
		var input = new List<IUseable>();
		Collider[] hits = Physics.OverlapSphere(t.position, maxUseRadius);

		foreach (Collider c in hits) {
			IUseable useable = c.GetComponent<IUseable>();
			if(useable != null && useable.UseEnabled()) {
				input.Add(useable);
			}
		}

		return input;
	}

	public void Chirp(ChirpType type){
		ImageKey chirpImage = ImageKey.None;

		switch (type) {
			case ChirpType.Command:{
				var chicks = FindObjectsOfType<ChickController>();

				if(chicks.Length == 0 || chicks[0].IsDead()){
					chirpImage = ImageKey.Sad;
				}else if(chicks[0].IsFullyGrown()){
					chirpImage = ImageKey.Happy;
				}else{
					chirpImage = (hunger > 0.4)?ImageKey.Happy:ImageKey.Sad;
				}

				foreach(var chick in chicks){
					chick.OnPlayerChirp(this);
				}

				break;
			}

			case ChirpType.Hungry:
				if(hunger < 0.35f){
					chirpImage = ImageKey.Sad;
				}else{
					chirpImage = ImageKey.Food;
				}
				break;

			case ChirpType.Happy:
				chirpImage = ImageKey.Happy;
				break;
			case ChirpType.Sad:
				chirpImage = ImageKey.Sad;
				break;

			default:
			break;
		}

		tweetEmitter.GetComponent<ParticleSystemRenderer>().material.SetTexture(0, ChirpImageManager.main.GetImage(chirpImage));

		AudioManager.main.PlaySoundEffect(SoundEffect.BirdChirp, t.position, chirpVol, chirpVol);

		tweetEmitter.Emit(1);
		chirpEmitter.Emit(1);
	}

	// Pass null as item to drop current carried item
	public void CarryItem(GameObject item){
		if(carriedItem) 
			carriedItem.transform.SetParent(null, true);

		carriedItem = item;

		if(item){
			item.transform.SetParent(mouthPos, true);
			item.transform.position = mouthPos.position;
			// Move to mouth
		}
	}

	public GameObject GetCarriedItem(){
		return carriedItem;
	}

	public void Feed(float amt){
		hunger += amt;
		hunger = Mathf.Min(hunger, 1f);

		Chirp(ChirpType.Happy);
	}
}
