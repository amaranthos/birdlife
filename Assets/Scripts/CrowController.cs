﻿using UnityEngine;
using System.Collections;

public enum CrowState{
	Scouting,
	Attacking,
}

public class CrowController : MonoBehaviour {
	public float flapForce = 1f;
	public float maxFlapRate = 4f;
	public float launchFlapMult = 4f;

	public float viewDist = 25f;
	public float fov = 50f;

	private float flapTimer = 0f;
	private float flapRate = 0f;

	private CrowState state = CrowState.Scouting;
	private ChickController chick;

	private Transform t;
	private Rigidbody r;
	private Animator a;
	private ParticleSystem tweetEmitter;
	private ParticleSystem chirpEmitter;
	private ParticleSystem featherEmitter;

	private PathFollower pathFollower;

	void Awake(){
		t = transform;
		r = GetComponent<Rigidbody>();
		a = GetComponentInChildren<Animator>();

		pathFollower = GetComponent<PathFollower>();

		var pems = GetComponentsInChildren<ParticleSystem>();
		chick = FindObjectOfType<ChickController>();

		foreach(var pem in pems){
			switch(pem.name){
				case "TweetEmitter":
					tweetEmitter = pem;
					break;
				case "ChirpEmitter":
					chirpEmitter = pem;
					break;
				case "FeatherEmitter":
					featherEmitter = pem;
					break;
			}
		}
		a.SetBool("IsFlying", true);
	}

	void Update(){
		flapTimer += Time.deltaTime;

		switch(state){
			case CrowState.Scouting: UpdateScouting(); break;
			case CrowState.Attacking: UpdateAttacking(); break;
			default: print("Unknown crow state"); break;
		}
	}

	/////////////////////////////// Update Scouting ///////////////////////////////
	void UpdateScouting(){
		if(!pathFollower.path) {
			Debug.LogError(name + " doesn't have path");
			Destroy(gameObject);
			return;
		}

		if(pathFollower.HasReachedPoint()) pathFollower.Next();
		var tpos = pathFollower.GetCurrent();

		FlyTowardPoint(tpos);

		if(ChickInView()){
			SetState(CrowState.Attacking);
		}
	}

	/////////////////////////////// Update Attacking ///////////////////////////////

	float chickNotSeenTimer = 0f;
	void UpdateAttacking(){
		if(!ChickInView()){
			chickNotSeenTimer -= Time.deltaTime;
		}else{
			chickNotSeenTimer = 7f;
		}

		if(chickNotSeenTimer <= 0f){
			SetState(CrowState.Scouting);
		}

		FlyTowardPoint(chick.transform.position);

	}


	void SetState(CrowState _state){
		state = _state;
	}


	/////////////////////////////// Common ///////////////////////////////

	Vector3 CalculateAvoidance(){
		var avoid = Vector3.zero;
		var p = t.position;

		avoid += CalculateAvoidanceInDir(p, t.forward, 10f);
		avoid += CalculateAvoidanceInDir(p,-t.forward, 4f);
		
		avoid += CalculateAvoidanceInDir(p, t.right, 7f);
		avoid += CalculateAvoidanceInDir(p,-t.right, 7f);

		avoid += CalculateAvoidanceInDir(p, Vector3.up, 2f);
		avoid += CalculateAvoidanceInDir(p,-Vector3.up, 5f);

		avoid += CalculateAvoidanceInDir(p, (t.forward + t.right).normalized, 7f)*0.6f;
		avoid += CalculateAvoidanceInDir(p, (t.forward - t.right).normalized, 7f)*0.6f;
		avoid += CalculateAvoidanceInDir(p, (t.forward + t.up).normalized, 7f)*0.6f;
		avoid += CalculateAvoidanceInDir(p, (t.forward - t.up).normalized, 7f)*0.6f;

		return avoid;
	}

	bool ChickInView(){
		var diff = chick.transform.position - t.position;
		var view = Vector3.Dot(diff, t.forward);

		if(view < Mathf.Cos(fov)) return false;
		if(diff.magnitude > viewDist) return false;

		return true;
	}

	Vector3 CalculateAvoidanceInDir(Vector3 pos, Vector3 dir, float dist){
		// Debug.DrawRay(pos, dir*dist, Color.green);

		RaycastHit hit;
		if(Physics.Raycast(pos, dir, out hit, dist)){
			return -dir * (1f - hit.distance/dist);
		}

		return Vector3.zero;
	}

	void FlyTowardPoint(Vector3 tpos){
		var tdiff = tpos - t.position;
		var tdir = tdiff.normalized;

		var avoid = CalculateAvoidance();
		float upness = Vector3.Dot(avoid, Vector3.up)*2f + Vector3.Dot(tdir, Vector3.up)*2f;
		float bank = Vector3.Dot(tdir, -t.right) + Vector3.Dot(avoid, -t.right)*2f;
		float fobst = Vector3.Dot(avoid, -t.forward);
		upness += fobst;

		float pitch = -Mathf.Clamp(upness, -1f, 1f) + Mathf.Clamp(Vector3.Dot(tdiff, t.forward), -1f, 1f);

		Bank(bank * 40f);
		Pitch(pitch * 10f);

		flapRate = Mathf.Clamp(upness * launchFlapMult, 0f, maxFlapRate);
		if(flapRate > 0f) Flap();
	}

	void Flap(){
		if(flapRate == 0f || flapTimer < 1f/flapRate) return;
		flapTimer = 0f;

		var altitude = t.position.y * 0.012f;
		var force = t.up * Mathf.Clamp(1f - altitude, 0f, 1f);
		force += t.forward * 0.1f;

		// DebugPanel.BindGroup("CrowFlap");
		// DebugPanel.Clear();
		// DebugPanel.Append("Flap power: ", flapForce, "N");

		r.AddForce(force * flapForce, ForceMode.Impulse);
		a.SetTrigger("Flap");
	}

	void Chirp(ChirpType type){
		// Change tweet tex to reflect type

		tweetEmitter.Emit(1);
		chirpEmitter.Emit(1);
		// Do other things here
	}

	void Bank(float amt){
		var curbank = t.rotation.eulerAngles.z;
		if(curbank > 180f) curbank -= 360f;

		t.rotation *= Quaternion.Euler(0f, -amt / 20f, (amt - curbank) *0.03f);
	}

	void Pitch(float angle){
		var curpitch = t.rotation.eulerAngles.x;
		if(curpitch > 180f) curpitch -= 360f;

		t.rotation *= Quaternion.Euler(angle - curpitch, 0f, 0f);
	}

	void Damage(){
		var go = Instantiate(featherEmitter.gameObject, t.position, Quaternion.identity) as GameObject;
		go.GetComponent<ParticleSystem>().Emit(500);
		go.AddComponent<DestroyAfterTime>().lifetime = go.GetComponent<ParticleSystem>().startLifetime+1f;
		Destroy(gameObject);
	}

	// void OnDrawGizmos(){
	// 	Gizmos.DrawRay(transform.position, Vector3.RotateTowards(transform.forward, transform.right, fov/90f, 0f) * viewDist);
	// 	Gizmos.DrawRay(transform.position, Vector3.RotateTowards(transform.forward, -transform.right, fov/90f, 0f) * viewDist);
	// 	Gizmos.DrawRay(transform.position, Vector3.RotateTowards(transform.forward, transform.up, fov/90f, 0f) * viewDist);
	// 	Gizmos.DrawRay(transform.position, Vector3.RotateTowards(transform.forward, -transform.up, fov/90f, 0f) * viewDist);
	// 	Gizmos.DrawRay(transform.position, transform.forward * viewDist);
	// }
}
