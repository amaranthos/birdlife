﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class GameManager : MonoBehaviour {
	static public GameManager main;
	public PlayerController player;
	public ChickController chick;

	public GameObject mainMenu;
	public GameObject restartMenu;
	public GameObject fogWalls;
	public GameObject startButton;
	public GameObject restartButton;

	public Color startGameFogWallColour;
	public Color endGameFogWallColour;

	public TextMesh deathText;
	private float fadeDeathTimer = -1f;
	private bool fadeDeathText = false;

	private bool isLevelStarted = false;
	private bool isGameOver = false;
	private bool canEnd = false;
	private bool lerpCamera = false;
	public float timeToStartLevel;
	private float timerToStartLevel;

	private Camera cam;
	private Vector3 campos = Vector3.zero;
	
	void Awake(){
		main = this;
		cam = Camera.main;
		player = FindObjectOfType<PlayerController>();
		chick = FindObjectOfType<ChickController>();
		
		ShowMenu(true);

		campos = cam.transform.position;

		player.enabled = false;
		chick.enabled = false;
	}

	void Start(){
		PostProcessingManager.main.SetWhiteOutHard(1f);
		PostProcessingManager.main.SetWhiteOut(0f);
		PostProcessingManager.main.SetBlackOut(false);
		EnvironmentManager.main.SetFogWallColor(startGameFogWallColour);
	}

	private void Update() {
		if(fadeDeathText){
			fadeDeathTimer += Time.deltaTime;
			var amt = fadeDeathTimer;

			var tc = deathText.color;
			tc.a = Mathf.Clamp(amt/4f-0.5f, 0f, 1f);
			deathText.color = tc;
		}

		if(!isLevelStarted && lerpCamera) {
			timerToStartLevel += Time.deltaTime; 

			Vector3 offset = Vector3.RotateTowards(-player.transform.forward, player.transform.up, player.cameraAngle*3.1415926f/180f, 0f);
			Vector3 targetPos = player.transform.position + offset * player.cameraDist;

			AudioManager.main.SetWindAmount(0.7f);

			float perc = timerToStartLevel/timeToStartLevel;

			cam.transform.position = Vector3.Lerp(campos, targetPos, perc);
			cam.transform.rotation = Quaternion.Slerp(cam.transform.rotation, 
				Quaternion.LookRotation((player.transform.position - cam.transform.position).normalized), 
				Time.deltaTime*(perc*perc*(perc+0.5f)*50f));

			if(timerToStartLevel >= timeToStartLevel) {
				lerpCamera = false;
				isLevelStarted = true;
				StartCoroutine(EnablePlayer());
				StartCoroutine(ChickChirpStart());
				timerToStartLevel = 0.0f;
			}
		}
	}

	IEnumerator ChickChirpStart() {
		yield return new WaitForSeconds(Random.Range(1.0f, 2.0f));
		chick.Chirp(ChirpType.Hungry);
		// yield return new WaitForSeconds(Random.Range(1.0f, 2.0f));
		// player.Chirp(ChirpType.Hungry);
	}

	IEnumerator EnablePlayer(){
		yield return new WaitForSeconds(0.1f);
		player.enabled = true;
		chick.enabled = true;		
	}

	public void OnPlayerDeath(string reason = "You Died"){
		isGameOver = true;

		print("Dead");
		player.gameObject.SetActive(false);
		PostProcessingManager.main.playerDeath = true;
		AudioManager.main.SetAmbientWindAmount(0f);
		AudioManager.main.HardSetWindAmount(0f);
		OffScreenIconManager.main.Clear();

		var c = Color.white;
		c.a = 0f;
		deathText.color = c;
		deathText.text = reason;
		fadeDeathText = true;

		StartCoroutine(EndGame());
	}

	public void OnChickDeath(bool starved = false){
		print("Chick Dead");
		print("Starved? " + starved);

		canEnd = true;
		PostProcessingManager.main.chickDeath = true;
		DropFogWalls();
		var c = Color.black;
		c.a = 0f;
		deathText.color = c;
		if(starved)
			deathText.text = "You left the chick to fend for itself";
		else
			deathText.text = "The chick died";

	}

	public void OnChickBecomesAdult(){
		print("Chick became adult");

		// canEnd = true;
		PostProcessingManager.main.chickSurvive = true;
		DropFogWalls();
		var c = Color.black;
		c.a = 0f;
		deathText.color = c;
		deathText.text = "The chick grew up and flew away";

		StartCoroutine(EndGameSequence());
	}

	public void OnPlayerLeave(){
		isGameOver = true;
		PostProcessingManager.main.playerLeave = true;
		OffScreenIconManager.main.Clear();
		StartCoroutine(EndGame());
	}

	void DropFogWalls(){
		fogWalls.transform.position -= Vector3.up * 30f;
		foreach(var c in fogWalls.GetComponentsInChildren<Collider>()){
			c.gameObject.tag = "EndFog";
		}

		EnvironmentManager.main.SetFogWallColor(endGameFogWallColour);
	}

	IEnumerator EndGameSequence(){
		yield return new WaitForSeconds(4f);
		OnPlayerLeave();
	}

	public void StartGame() {
		lerpCamera = true;
		ShowMenu(false);
	}

	public void RestartGame() {
		Application.LoadLevel(0);
	}

	public void QuitGame() {
		Application.Quit();
	}

	public void ShowMenu(bool show) {
		mainMenu.SetActive(show);
		EventSystem.current.SetSelectedGameObject(startButton);
	}

	IEnumerator EndGame(){
		fadeDeathText = true;

		yield return new WaitForSeconds(14f);
		// restartMenu.SetActive(true);
		// EventSystem.current.SetSelectedGameObject(restartButton);
		RestartGame();
	}

	public bool GameCanEnd(){
		return canEnd;
	}

	public bool IsGameOver(){
		return isGameOver;
	}
}
