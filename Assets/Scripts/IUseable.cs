﻿using UnityEngine;
using System.Collections;

public abstract class IUseable : MonoBehaviour {
	public abstract void Use(PlayerController user);
	public virtual bool UseEnabled(){
		return true;
	}

	// UseableType GetUseableType(); // May not actually be necessary
}

public enum UseableType {
	Twig,
	Nest,
}