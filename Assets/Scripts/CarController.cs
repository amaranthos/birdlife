﻿using UnityEngine;
using System.Collections;

public class CarController : MonoBehaviour {
	public float speed = 6f;
	private bool carInWay = false;

	private Transform t;
	private PathFollower pf;

	void Awake () {
		t = transform;
		pf = GetComponent<PathFollower>();
	}

	void Update(){
		if(pf.HasReachedPoint()) pf.Next();
		var pt = pf.GetCurrent();

		t.LookAt(new Vector3(pt.x, t.position.y, pt.z));

		if(!carInWay)
			t.Translate(Vector3.forward*speed*Time.deltaTime);
	}

	void OnTriggerStay(Collider c){
		if(c.CompareTag("Car")){
			carInWay = true;
		}
	}

	void OnTriggerExit(Collider c){
		if(c.CompareTag("Car")){
			carInWay = false;
		}	
	}
}
