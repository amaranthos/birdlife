﻿using UnityEngine;
using UnityEngine.Audio;
using System.Collections;
using System.Collections.Generic;

public enum SoundEffect {
	None,
	ChickChirp,
	BirdChirp,
	BirdFlap,
	BirdLand,
	BirdDie,
}

[System.Serializable]
public class SoundItem {
	public SoundEffect key;
	public AudioClip clip;
}

public class AudioManager : MonoBehaviour {
	static public AudioManager main;

	public AudioMixer ambienceMixer;
	public List<SoundItem> sounds = new List<SoundItem>();
	public List<AudioClip> winds = new List<AudioClip>();

	private float windAmt;
	private float targetWindAmt = 0f;

	private float rainAmt;
	private float altitude = 0f;
	private float targetRainAmt = 0f;

	private void Awake(){
		main = this;
	}

	void Start(){
		SetAmbientWindAmount(1f);
	}

	void Update(){
		windAmt = Mathf.Lerp(windAmt, targetWindAmt, Time.deltaTime*2f);
		rainAmt = Mathf.Lerp(rainAmt, targetRainAmt, Time.deltaTime/2f);

		var medWindAmt = 1f - Mathf.Pow(2f, -6f * windAmt);
		var heavyWindAmt = 1f - Mathf.Pow(2f, -7f * (windAmt - 0.5f));

		ambienceMixer.SetFloat("LightWindVol", Mathf.Clamp(Mathf.Pow(2f, -0.5f * altitude), 0f, 1f)*80f - 80f);
		ambienceMixer.SetFloat("MedWindVol", Mathf.Clamp(medWindAmt, 0f, 1f)*80f - 80f);
		ambienceMixer.SetFloat("HeavyWindVol", Mathf.Clamp(heavyWindAmt, 0f, 1f)*80f - 80f);

		var rainMixAmt = 20f;
		var groundRain = rainAmt * ((1f-altitude) * rainMixAmt + (80f - rainMixAmt));
		var middleRain = rainAmt * 80f;

		ambienceMixer.SetFloat("GroundRainVol", groundRain*0.9f - 80f);
		ambienceMixer.SetFloat("MiddleRainVol", middleRain - 80f);
	}

	public void PlaySoundEffect(SoundEffect effect, Vector3 position, float volMin = 0.5f, float volMax = 1.0f) {
		if(effect == SoundEffect.None) return;
		AudioSource.PlayClipAtPoint(sounds.Find(item=>item.key==effect).clip, position, Random.Range(volMin, volMax + Mathf.Epsilon));
	}
	
	public void SetWindAmount(float amt){
		targetWindAmt = amt;
	}
	
	public void HardSetWindAmount(float amt){
		targetWindAmt = windAmt = amt;
	}
	
	public void SetAmbientWindAmount(float amt){
		amt = Mathf.Clamp(amt, 0f, 1f);

		ambienceMixer.SetFloat("WindVol", amt*80f - 80f);
	}
	
	public void SetRainAmount(float amt){
		targetRainAmt = amt;
	}
	public void SetAltitude(float alt){
		altitude = Mathf.Clamp(alt, 0f, 1f);
	}
}


