﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Path : MonoBehaviour {
	public List<Transform> waypoints = new List<Transform>();

	public int GetNearestIndex(Vector3 p){
		int min = -1;
		float mind = Mathf.Infinity;

		for(int i = 0; i < waypoints.Count; i++){
			var d = Vector3.Distance(waypoints[i].position, p);
			if(d < mind){
				mind = d;
				min = i;
			}
		}

		if(min == -1){
			Debug.Log("No nearest waypoint in path");
		}

		return min;
	}

	public Vector3 GetWaypoint(int index){
		if(index >= 0 && index < waypoints.Count){
			return waypoints[index].position;
		}

		return Vector3.zero;
	}
}
