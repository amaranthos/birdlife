﻿using UnityEngine;
using System.Collections;

public class EnvironmentManager : MonoBehaviour {
	static public EnvironmentManager main;

	private float counter = 0.45f;
	private float counter2 = 0.45f;
	private float hour;

	public Light sun;
	public Light moon;
	public float dayLength;

	public Gradient sunTint;
	public AnimationCurve moonBrightness;
	public Gradient ambient;
	public Gradient fog;
	public Gradient skyboxTop;
	public Gradient skyboxBottom;
	public Color endgameSkyboxTop;
	public Color endgameSkyboxBottom;

	public Vector2 clearPeriod;
	public Vector2 rainPeriod;

	public ParticleSystem rain;
	public GameObject fogWall;

	public GameObject playerDustSystem;
	private ParticleSystem[] playerDusts;

	private float weatherTimer = 0.0f;
	private float weatherProgress = 0.0f;
	private bool isRaining = false;

	private void Awake(){
		main = this;
	}

	private void Start() {
		playerDusts = playerDustSystem.GetComponentsInChildren<ParticleSystem>();

		dayLength *= 60.0f;
		clearPeriod *= 60.0f;
		rainPeriod *= 60.0f;
	}

	private void Update() {
		DisplayTimeOfDay();
		DetermineRain();

		var player = GameManager.main.player;
		var rainPos = rain.transform.position;
		var predPlayerPos = player.transform.position + player.GetComponent<Rigidbody>().velocity*1f; // predict 3s

		rainPos.x = predPlayerPos.x;
		// rainPos.y = predPlayerPos.y + 10f;
		rainPos.z = predPlayerPos.z;
		rain.transform.position = rainPos;
	}

	private void DisplayTimeOfDay() {
		if(counter >= 1.0f) {
			counter = 0.0f;
		}

		hour = counter * 24f;

		sun.transform.localEulerAngles = new Vector3((counter * 360.0f) - 90f, 0f, 0f);
		counter += Time.deltaTime / dayLength;
		// sun.color = sunTint.Evaluate(counter * 2f);
		sun.color = sunTint.Evaluate(hour/24f);

		if(counter < 0.5f) {
			counter2 = counter;
		}

		if(counter > 0.5f) {
			counter2 = (1f - counter);
		}

		sun.intensity = (counter2 - 0.1f) * 1.7f;
		moon.intensity = moonBrightness.Evaluate(hour/24f);
		DebugPanel.SetGroup("EnvironmentManager", "Moon: ", moon.intensity);

		RenderSettings.ambientSkyColor = skyboxTop.Evaluate(hour/24.0f) * 0.5f;
		RenderSettings.fogColor = fog.Evaluate(hour/24.0f);

		if(GameManager.main.GameCanEnd()){
			var bottom = RenderSettings.skybox.GetColor("_Color1");
			var top = RenderSettings.skybox.GetColor("_Color2");

			bottom = Color.Lerp(bottom, endgameSkyboxBottom, Time.deltaTime);
			top = Color.Lerp(top, endgameSkyboxTop, Time.deltaTime);

			RenderSettings.skybox.SetColor("_Color1", bottom);
			RenderSettings.skybox.SetColor("_Color2", top);
		}else{
			RenderSettings.skybox.SetColor("_Color1", skyboxBottom.Evaluate(hour/24.0f));
			RenderSettings.skybox.SetColor("_Color2", skyboxTop.Evaluate(hour/24.0f));
		}
	}

	private void DetermineRain() {
		if(weatherTimer <= 0.0f) {
			if(isRaining) {
				weatherTimer = Random.Range(rainPeriod.x, rainPeriod.y + Mathf.Epsilon);
			} else {
				weatherTimer = Random.Range(clearPeriod.x, clearPeriod.y + Mathf.Epsilon);
			}
		}

		weatherProgress += Time.deltaTime;

		if(weatherProgress >= weatherTimer) {
			if(!isRaining) {
				StartRain();
			}
			else {
				StopRain();
			}

			weatherProgress = weatherTimer = 0.0f;
		}
	}

	IEnumerator SetRainParams(float p){
		yield return new WaitForSeconds(2f);

		AudioManager.main.SetRainAmount(p);
		PostProcessingManager.main.rainParam = p;
	}

	private void StartRain() {
		StartCoroutine(SetRainParams(1f));
		rain.enableEmission = true;
		foreach(var ps in playerDusts) ps.enableEmission = false;
		isRaining = true;
	}

	private void StopRain()  {
		StartCoroutine(SetRainParams(0f));
		rain.enableEmission = false;
		foreach(var ps in playerDusts) ps.enableEmission = true;
		isRaining = false;
	}

	public void SetFogWallColor(Color col){
		foreach(var fw in fogWall.GetComponentsInChildren<ParticleSystem>()){
			fw.startColor = col;
		}
	}
}
