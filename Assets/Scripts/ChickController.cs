﻿using UnityEngine;
using System.Collections;

public class ChickController : MonoBehaviour {
	public PlayerController player = null;
	private bool followingPlayer = false;
	private bool canFly = false;
	private bool fullyGrown = false;
	private bool dead = false;

	private Transform t;
	private Rigidbody r;
	private Animator a;
	private ParticleSystem tweetEmitter;
	private ParticleSystem chirpEmitter;

	public float timeToFullyGrown = 3f;
	private float timeAlive = 0f;

	public float hunger = 0.3f;
	public float hungerFlightThreshold = 0.4f;
	public float maxFlapForce = 0.5f;
	public float hungerPeriod = 0.75f; // Days till starve
	
	public float hungerChirpThreshold = 0.65f;
	public Vector2 hungerChirpPeriod = new Vector2(10f, 30f); // Seconds
	private float hungerChirpTimer = 0f;

	public float flapTimer = 0f;
	public float flapRate = 0f;
	private float flapPower = 0f;

	public float chirpVol = 0.2f;
	public float targetDistFromTarget = 0.5f;

	public SpriteRenderer aboveHeadIcon;
	private OffScreenIcon locator;

	private Vector3 target;

	void Awake(){
		t = transform;
		r = GetComponent<Rigidbody>();
		a = GetComponent<Animator>();
		
		var pems = GetComponentsInChildren<ParticleSystem>();

		foreach(var pem in pems){
			switch(pem.name){
				case "TweetEmitter":
					tweetEmitter = pem;
					break;
				case "ChirpEmitter":
					chirpEmitter = pem;
					break;
			}
		}

		player = FindObjectOfType<PlayerController>();

		aboveHeadIcon.transform.parent = null; // Free it so we can smooth its movement
	}

	void Start() {
		locator = OffScreenIconManager.main.NewIcon(ImageKey.Locate, t.position, true);
		locator.depth = 1f;
		target = t.position;
	}

	void Update () {
		timeAlive += Time.deltaTime;
		flapTimer += Time.deltaTime;
		hunger -= Time.deltaTime / (hungerPeriod * EnvironmentManager.main.dayLength);

		UpdateAboveHeadIcon();

		locator.worldPosition = t.position;

		if(dead) return;

		if(hunger < 0f && !dead) {
			GameManager.main.OnChickDeath(true);
			Chirp(ChirpType.Death);
			var tex = ChirpImageManager.main.GetImage(ImageKey.Death);
			aboveHeadIcon.sprite = Sprite.Create(tex, new Rect(0,0,tex.width,tex.height), new Vector2(0.5f, 0.5f), tex.width/2f);
			dead = true;
			return;
		}
		if(!fullyGrown && timeAlive >= timeToFullyGrown * EnvironmentManager.main.dayLength) {
			fullyGrown = true;
			GameManager.main.OnChickBecomesAdult();
		}

		if(fullyGrown){
			canFly = true;
			hunger = 1f;
			flapPower = 1f;
			FlyTowardPoint(new Vector3(100f, 200f, 0f));

			return;
		}
		
		hungerChirpTimer -= Time.deltaTime;
		if(hunger < hungerChirpThreshold && hungerChirpTimer < 0f){
			Chirp(ChirpType.Hungry);

			hungerChirpTimer = Random.Range(hungerChirpPeriod.x, hungerChirpPeriod.y);
		}

		DebugPanel.SetGroup("ChickHunger", hunger*100f, "%");

		canFly = hunger > hungerFlightThreshold;

		if(followingPlayer)
			UpdateFollow();

		// Look at player when not moving
		if(r.velocity.magnitude < 0.1f){
			var p = player.transform.position;
			p.y = t.position.y;
			t.LookAt(p);
		}
	}

	void UpdateFollow(){
		if(!canFly) return;
		var pdiff = (target - t.position);
		var pdist = pdiff.magnitude;
		var pdir = pdiff.normalized;
		var velTowardTarget = Vector3.Dot(pdir, r.velocity);

		var perpvel = Vector3.Cross(pdir, t.right);
		if(perpvel.y < 0f) perpvel = -perpvel;
		Debug.DrawRay(t.position, perpvel*2f, Color.magenta);

		if(pdist > targetDistFromTarget*Mathf.Clamp(velTowardTarget, 2f, 5f)){
			t.LookAt(target); 

			var avoidance = CalculateAvoidance();
			Debug.DrawRay(t.position, avoidance, Color.red);

			var distMod = Mathf.Clamp(pdist/4f, 0.1f, 1f);
			
			flapPower = distMod;
			flapRate = Mathf.Max(Vector3.Dot(avoidance, Vector3.up)*3f, 0f)*hunger*hunger;

			DebugPanel.SetGroup("ChickAvoid", "Avoidance vector: ", avoidance);

			Flap();
		}else if(pdist > targetDistFromTarget){
			t.LookAt(perpvel);
			flapPower = targetDistFromTarget - pdist;
			flapRate = Mathf.Clamp(velTowardTarget, 0.1f, 2f);

			Flap();
		}

		DebugPanel.SetGroup("ChickUpdate", "Flap Rate: ", flapRate);
		DebugPanel.AppendGroup("ChickUpdate", "Flap Power: ", flapPower);
	}

	void UpdateAboveHeadIcon(){
		// Position
		var target = t.position + Vector3.up * (0.2f + Mathf.Sin(Time.time*2f)*0.02f);
		var aht = aboveHeadIcon.transform;

		aht.position = Vector3.Lerp(aht.position, target, Time.deltaTime*8f);

		// Alpha
		var ac = aboveHeadIcon.color;
		ac.a = Mathf.Lerp(ac.a, 1f - Mathf.Clamp(hunger/hungerChirpThreshold, 0f, 1f), Time.deltaTime*2f);
		aboveHeadIcon.color = ac;
	}

	Vector3 CalculateAvoidance(){
		var avoid = Vector3.zero;
		var p = t.position;

		avoid += CalculateAvoidanceInDir(p, t.forward, 2f);
		
		avoid += CalculateAvoidanceInDir(p, t.right, 3f);
		avoid += CalculateAvoidanceInDir(p,-t.right, 3f);

		avoid += CalculateAvoidanceInDir(p, Vector3.up, 1f);
		avoid += CalculateAvoidanceInDir(p,-Vector3.up, 3f);

		avoid += CalculateAvoidanceInDir(p, (t.forward + t.right * 0.4f).normalized, 1f)*0.3f;
		avoid += CalculateAvoidanceInDir(p, (t.forward - t.right * 0.4f).normalized, 1f)*0.3f;

		return avoid;
	}

	Vector3 CalculateAvoidanceInDir(Vector3 pos, Vector3 dir, float dist){
		Debug.DrawRay(pos, dir, Color.green);

		RaycastHit hit;
		if(Physics.Raycast(pos, dir, out hit, dist)){
			return -dir * (1f - hit.distance/dist);
		}

		return Vector3.zero;
	}

	void Flap(){
		if(flapRate == 0f || flapTimer < 1f/flapRate) return;
		flapTimer = 0f;

		var altitude = t.position.y * 0.012f;
		var force = t.up * Mathf.Clamp(1f - altitude, 0f, 1f);
		force += t.forward * 0.1f;

		var flightPower = (hunger - hungerFlightThreshold)/(1f - hungerFlightThreshold);
		flightPower = Mathf.Clamp((flightPower*0.5f + 0.5f) * flapPower, 0f, maxFlapForce);

		DebugPanel.SetGroup("ChickFlap", "Flap power: ", flightPower, "N");

		r.AddForce(force * flightPower, ForceMode.Impulse);
	}

	public void OnPlayerChirp(PlayerController pc){
		StartCoroutine(Respond());
	}

	IEnumerator Respond(){
		yield return new WaitForSeconds(0.2f);

		RaycastHit hit;
		if(followingPlayer)
		if(Physics.Raycast(player.transform.position, -Vector3.up, out hit)){
			target = hit.point;
		}else{
			target = player.transform.position;
		}

		Chirp(ChirpType.Acknowledge);
	}

	public void Chirp(ChirpType type){
		// if(dead) return;
		ImageKey chirpImage = ImageKey.None;

		switch (type) {
			case ChirpType.Acknowledge:{
				if(dead)
					chirpImage = ImageKey.Death;
				else if(fullyGrown)
					chirpImage = ImageKey.Happy;
				else
					chirpImage = (followingPlayer && (hunger > 0.4))?ImageKey.Happy:ImageKey.Sad;

				break;
			}

			case ChirpType.Hungry:
				if(hunger < 0.35f){
					chirpImage = ImageKey.Sad;
				}else{
					chirpImage = ImageKey.Food;
				}
				break;

			case ChirpType.Happy:
				chirpImage = ImageKey.Happy;
				break;
			case ChirpType.Sad:
				chirpImage = ImageKey.Sad;
				break;
			case ChirpType.Death:
				chirpImage = ImageKey.Death;
				break;

			default:
			break;
		}

		tweetEmitter.GetComponent<ParticleSystemRenderer>().material.SetTexture(0, ChirpImageManager.main.GetImage(chirpImage));
		OffScreenIconManager.main.NewIcon(chirpImage, t.position, chirpImage == ImageKey.Death);

		if(chirpImage != ImageKey.Death)
			AudioManager.main.PlaySoundEffect(SoundEffect.ChickChirp, t.position, chirpVol, chirpVol);

		tweetEmitter.Emit(1);
		if(chirpImage != ImageKey.Death)
			chirpEmitter.Emit(1);
		// Do other things here
	}

	public void Feed(float amt){
		if(dead) return;

		hunger += amt;
		hunger = Mathf.Min(hunger, 1f);

		followingPlayer = true;

		Chirp(ChirpType.Happy);
	}

	void FlyTowardPoint(Vector3 tpos){
		var tdiff = tpos - t.position;
		var tdir = tdiff.normalized;

		var avoid = CalculateAvoidance();
		float upness = Vector3.Dot(avoid, Vector3.up)*3f + Vector3.Dot(tdir, Vector3.up)*2f;// Mathf.Clamp(tdiff.y*0.3f, 0f, 1f);
		float bank = Vector3.Dot(tdir, -t.right) + Vector3.Dot(avoid, -t.right)*4f;
		float fobst = Vector3.Dot(avoid, -t.forward);
		upness += fobst;

		float pitch = -Mathf.Clamp(upness, -1f, 1f) + Mathf.Clamp(Vector3.Dot(tdiff, t.forward), -1f, 1f);

		Bank(bank * 40f);
		Pitch(pitch * 10f);

		flapRate = Mathf.Clamp(upness, 0f, 10f);
		if(flapRate > 0f) Flap();
	}

	void Pitch(float angle){
		var curpitch = t.rotation.eulerAngles.x;
		if(curpitch > 180f) curpitch -= 360f;

		t.rotation *= Quaternion.Euler(angle - curpitch, 0f, 0f);
	}

	void Bank(float amt){
		var curbank = t.rotation.eulerAngles.z;
		if(curbank > 180f) curbank -= 360f;

		t.rotation *= Quaternion.Euler(0f, -amt / 20f, (amt - curbank) *0.03f);
	}

	void OnTriggerEnter(Collider col){
		if(!dead && col.CompareTag("DeathBox")){
			GameManager.main.OnChickDeath();
			Chirp(ChirpType.Death);
			locator.SetIcon(ImageKey.Death);
			dead = true;
		}
	}

	void OnTriggerStay(Collider col){
		if(dead) return;

		if(col.CompareTag("FlapVol")){
			var fwd = t.forward;
			fwd.y = 0f;
			fwd = fwd.normalized;
			t.LookAt(t.position + fwd);

			r.AddForce(Vector3.up*3f);

			Flap();
		}
	}

	public bool IsFollowingPlayer(){
		return followingPlayer;
	}

	public bool IsFullyGrown(){
		return fullyGrown;
	}

	public bool IsDead(){
		return dead;
	}
}
