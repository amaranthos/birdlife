﻿using UnityEngine;
using System.Collections;

public class Billboard : MonoBehaviour {
	private Transform t;

	void Awake(){
		t = transform;
	}
	
	void Update () {
		var c = Camera.main.transform;
		t.LookAt(t.position + c.rotation * Vector3.back, c.rotation * Vector3.up);
	}
}
