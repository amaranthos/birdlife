﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour {

	public Menu current;


	public void Start() {
		ShowMenu(current);
	}

	public void ShowMenu(Menu menu) {
		if (current != null) current.IsOpen = false;

		Debug.Log("Set menu " + menu.name + " open");

		current = menu;
		current.IsOpen = true;
	}
}
