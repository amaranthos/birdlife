using UnityEngine;
using System.Collections;

public class FlightDynamics : MonoBehaviour {
	private Transform t;
	private Rigidbody r;

	public float wingSpan = 32f;
	public float wingWidth = 5f;

	void Awake(){
		t = transform;
		r = GetComponent<Rigidbody>();
	}

	void FixedUpdate(){
		// http://en.wikipedia.org/wiki/Gliding_flight
		// http://www.blitzbasic.com/Community/posts.php?topic=25776
		// http://en.wikipedia.org/wiki/Lift_(force)
		// http://en.wikipedia.org/wiki/Flight_dynamics_(fixed-wing_aircraft)
		
		// http://www.grc.nasa.gov/WWW/K-12/airplane/lifteq.html
		// http://www.grc.nasa.gov/WWW/K-12/airplane/incline.html

		var fwd = t.forward;
		var vel = r.velocity;
		var nvel = r.velocity.normalized;
		var fwdVel = Vector3.Dot(fwd, vel);

		if(vel.magnitude < 0.001f) return;

		var declination = Vector3.Angle(fwd, vel);
		// var angleOfAttack = (-t.rotation.eulerAngles.x - 5f) * Mathf.PI / 180f;
		var angleOfAttack = (declination - t.rotation.eulerAngles.x) * Mathf.PI / 180f;
		while(angleOfAttack > Mathf.PI) angleOfAttack -= Mathf.PI * 2f;
		while(angleOfAttack < -Mathf.PI) angleOfAttack += Mathf.PI * 2f;

		var airDensity = 1.229f * 3f;
		var wingSpanM = wingSpan/100f;
		var wingWidthM = wingWidth/100f;
		var wingArea = wingSpanM * wingWidthM; // 32cm x 5cm
		var liftCoefficient = 2f * Mathf.PI * angleOfAttack * 2.5f + 0.3f;
		// var liftCoefficient = Vector3.Dot(fwd, vel);

		var lift = Mathf.Max(liftCoefficient * airDensity * fwdVel / 2f * wingArea, 0f);

		var initialDragCoefficient = 0.045f * 6f;
		var dragCoefficient = initialDragCoefficient;
		var drag = dragCoefficient * airDensity * fwdVel * fwdVel / 2f * wingArea;

		var up = Vector3.Cross(nvel, t.right);
		// r.AddForce(up*lift - fwd*drag);
		r.AddForce(up*lift - nvel*drag + Vector3.Scale(fwd-nvel, Vector3.one - Vector3.up).normalized*1f);

		// if(GetComponent<PlayerController>() != null){
		// 	DebugPanel.BindGroup("FlightDynamics");
		// 	DebugPanel.Clear();
		// 	DebugPanel.Append("Angle of attack: ", angleOfAttack*180f/Mathf.PI, "°");
		// 	DebugPanel.Append("Speed: ", r.velocity.magnitude, "m/s");
		// 	DebugPanel.Append("Fwd Speed: ", fwdVel, "m/s");
		// 	DebugPanel.Break();
		// 	DebugPanel.Append("Cl: ", liftCoefficient);
		// 	DebugPanel.Append("Cd: ", dragCoefficient);
		// 	DebugPanel.Break();
		// 	DebugPanel.Append("Lift: ", lift);
		// 	DebugPanel.Append("Drag: ", drag);
		// 	DebugPanel.Break();
		// 	DebugPanel.Append("L/D: ", lift/drag);
		// }

		// Debug.DrawRay(t.position, vel);
		// Debug.DrawRay(t.position, up*lift);
		// Debug.DrawRay(t.position, -nvel*drag);
	}
}
