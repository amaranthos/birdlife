﻿using UnityEngine;
using System.Collections;

public class Food : IUseable {
	public float feedAmount = 0.5f;
	public GameObject lightBeam;

	public override void Use(PlayerController pc){
		if(pc.GetCarriedItem() != gameObject){
			pc.CarryItem(gameObject);
			GetComponent<Collider>().enabled = false;
			lightBeam.SetActive(false);
		}else{
			var chick = GameManager.main.chick;
			var cpos = chick.transform.position;

			if(Vector3.Distance(cpos, transform.position) < 2f){
				chick.Feed(feedAmount);
			}else{
				pc.Feed(feedAmount);
			}

			Destroy(gameObject);
			pc.CarryItem(null);
		}
	}
}
