﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(Path))]
public class PathEditor : Editor {
	float waypointHandleSize = 2f;

	Path path;
	Vector3 pos = Vector3.zero;

	public override void OnInspectorGUI(){
		DrawDefaultInspector();

		if(GUILayout.Button("Subdivide")) {
			Subdivide();
		}

		if(GUILayout.Button("Clear Path")) {
			path.waypoints.Clear();
		}
	}

	void OnSceneGUI(){
		int controlId = GUIUtility.GetControlID(FocusType.Passive);
		var e = Event.current;

		path = target as Path;

		switch(e.type){
			case EventType.MouseDown:
				if(e.button != 0) break;

				if(e.shift){
					AddWaypoint(pos);
					e.Use();
					GUIUtility.hotControl = controlId;
				}else if(e.control){
					DeleteWaypoint(pos);
					e.Use();
					GUIUtility.hotControl = controlId;
				}

				break;

			case EventType.MouseMove:
				Vector2 mousePos = e.mousePosition;

				var ray = HandleUtility.GUIPointToWorldRay(mousePos);
				RaycastHit hit;
				if(Physics.Raycast(ray, out hit, 1000f)){
					pos = hit.point;
				}

				break;

			case EventType.MouseUp:
				if(e.button != 0) break;

				if(GUIUtility.hotControl == controlId){
					GUIUtility.hotControl = 0;
					e.Use();
				}

				break;

			default: break;
		}

		ProjectWaypoint(pos);
		DrawWaypoints();
		HandleUtility.Repaint();
	}

	void AddWaypoint(Vector3 position){
		var wp = new GameObject("Waypoint");
		wp.AddComponent<Waypoint>();
		var wpt = wp.transform;
		wpt.parent = path.transform;
		wpt.position = position;
		path.waypoints.Add(wpt);
	}

	void DeleteWaypoint(Vector3 position){
		foreach(var w in path.waypoints){
			RaycastHit hit;
			if(Physics.Raycast(w.position, -Vector3.up, out hit, 1000f)
				&& Vector3.Distance(position, hit.point) < waypointHandleSize
				|| Vector3.Distance(w.position, position) < waypointHandleSize){

				DestroyImmediate(w.gameObject);
			}	
		}
		path.waypoints.RemoveAll(w => w == null);
	}

	void ProjectWaypoint(Vector3 position){
		float hs = waypointHandleSize;

		if(Event.current.shift){
			Handles.color = Color.green;
		}else if(Event.current.control){
			Handles.color = Color.red;
		}else{
			Handles.color = Color.blue;
			hs *= 0.3f;
		}

		Handles.DrawWireDisc(position + Vector3.up*0.1f, Vector3.up, hs);
	}

	void DrawWaypoints(){
		if(path.waypoints.Count == 0) return;

		Vector3[] points = new Vector3[path.waypoints.Count + 1];
		uint i = 0;

		Handles.color = Color.cyan;
		foreach(var w in path.waypoints){
			var p = w.position + Vector3.up*0.1f;
			Handles.Label(p, "    " + i);
			Handles.DrawWireDisc(p, Vector3.up, waypointHandleSize);

			RaycastHit hit;
			if(Physics.Raycast(p, -Vector3.up, out hit, 1000f)){
				if(Vector3.Distance(p, hit.point) > 0.5f){
					Handles.DrawLine(p, hit.point);
					Handles.DrawWireDisc(hit.point, hit.normal, waypointHandleSize);
				}
			}

			w.position = Handles.PositionHandle(w.position, Quaternion.identity);

			points[i++] = p;
		}

		points[i++] = points[0];

		Handles.color = Color.cyan;
		Handles.DrawPolyLine(points);
	}

	void Subdivide(){
		if(path.waypoints.Count < 2) return;

		var wps = path.waypoints;
		path.waypoints = new List<Transform>();

		Vector3 p1, p2, np;

		for(int i = 0; i < wps.Count-1; i++){
			p1 = wps[i].position;
			p2 = wps[i+1].position;
			np = Vector3.Lerp(p1, p2, 0.5f);

			AddWaypoint(p1);
			AddWaypoint(np);
		}

		p1 = wps[wps.Count-1].position;
		p2 = wps[0].position;
		np = Vector3.Lerp(p1, p2, 0.5f);
		AddWaypoint(p1);
		AddWaypoint(np);
	}
}
