﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;

[System.Serializable]
public struct RangeI {
	public int min;
	public int max;

	public RangeI(int min, int max) {
		this.min = min;
		this.max = max;
	}

	public int Rand {
		get {return Random.Range(min, max + 1);}
	}
}

[System.Serializable]
public struct RangeF {
	public float min;
	public float max;
	
	public RangeF(float min, float max) {
		this.min = min;
		this.max = max;
	}
	
	public float Rand {
		get {return Random.Range(min, max + float.Epsilon);}
	}
}

public class ClutterInfo {

	public ClutterInfo(RangeI count, RangeF distance, RangeF scale, RangeF xRot, RangeF yRot, RangeF zRot) {
		this.count = count;
		this.distance = distance;
		this.scale = scale;
		this.xRot = xRot;
		this.yRot = yRot;
		this.zRot = zRot;
	} 

	public RangeI count;
	public RangeF distance;
	public RangeF scale;
	public RangeF yScale;
	public RangeF zScale;
	public RangeF xRot;
	public RangeF yRot;
	public RangeF zRot;
}

[ExecuteInEditMode]
public class ClutterPlacerWindow : EditorWindow {
	private static ClutterPlacerWindow window;
	private static GameObject obj;

	private ClutterInfo med = new ClutterInfo(new RangeI(1, 3), new RangeF(5, 6), new RangeF(0.9f, 1.1f), new RangeF(-5, 5), new RangeF(-360, 360), new RangeF(-5, 5));
	private ClutterInfo small = new ClutterInfo(new RangeI(1, 9), new RangeF(4, 6), new RangeF(0.9f, 1.1f), new RangeF(-5, 5), new RangeF(-360, 360), new RangeF(-5, 5));

	private bool showMed = true;
	private bool showSmall = true;

	private bool showMedList = true;
	private bool showSmallList = true; 

	private List<GameObject> medObjs = new List<GameObject>();
	private List<GameObject> smallObjs = new List<GameObject>();

	private List<GameObject> output = new List<GameObject>();

	private Vector2 scrollPos;

	[MenuItem("Tools/ClutterPlacer")]
	public static void Init() {
		window = (ClutterPlacerWindow)EditorWindow.GetWindow(typeof(ClutterPlacerWindow));
		window.title = "Clutter Placer";
		window.minSize = new Vector2(300, 200);
	}

	private void Update() {
		Repaint();
	}

	private void OnGUI() {
		if(window == null) {
			Init();
		}

		if(Selection.activeGameObject != null && Selection.activeGameObject.tag == "Large_Obj") {
			obj = Selection.activeGameObject;
		}
		else {
			obj = null;
		}

		if(obj){
			this.RemoveNotification();

			EditorGUI.BeginChangeCheck();
			scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

			EditorGUILayout.LabelField("Currently Selected: " + obj.name, EditorStyles.boldLabel);

			if(GUILayout.Button(new GUIContent("Place Clutter", "Will generate and place medium and small clutter prefabs based on current settings"), GUILayout.Height(50), GUILayout.Width(250))) {
			
				PlaceClutter();
			}

			if(GUILayout.Button(new GUIContent("Relax Constraints", "Shuffle the objects apart to resolve collisions, may need to be run multiple times "), GUILayout.Height(50), GUILayout.Width(250))) {
			
				RelaxConstraints();
			}

			EditorGUILayout.Space();

			showMed = EditorGUILayout.Foldout(showMed, new GUIContent("Medium Object Settings", "These settings will affect the layout of medium objects around the selected large object"));
			if(showMed){
				EditorGUI.indentLevel+=1;
				DrawInfoSettings(med);
				EditorGUI.indentLevel-=1;
			}

			showSmall = EditorGUILayout.Foldout(showSmall, new GUIContent("Small Object Settings", "These settings will affect the layout of small objects around the selected large object"));
			if(showSmall){
				EditorGUI.indentLevel+=1;
				DrawInfoSettings(small);
				EditorGUI.indentLevel-=1;
			}

			showMedList = EditorGUILayout.Foldout (showMedList, new GUIContent("Medium Object List", "Drag and drop prefabs into here that you wish to be placed as medium-sized objects"));
			if(showMedList) {
				EditorGUI.indentLevel+=1;
				DrawListGameObjs(medObjs);
				EditorGUI.indentLevel-=1;
			}

			showSmallList = EditorGUILayout.Foldout (showSmallList, new GUIContent("Small Object List", "Drag and drop prefabs into here that you wish to be placed as small-sized objects"));
			if(showSmallList) {
				EditorGUI.indentLevel+=1;
				DrawListGameObjs(smallObjs);
				EditorGUI.indentLevel-=1;
			}

			EditorGUILayout.EndScrollView();

			if(EditorGUI.EndChangeCheck()) {

			}
		}
		else {
			this.ShowNotification(new GUIContent("Please select a game object tagged with 'Large_Obj'"));
		}
	}

	private void DrawListGameObjs(List<GameObject> list) {
		int count = list.Count;
		count = Mathf.Max(EditorGUILayout.IntField("Size", count), 0);
		
		if(count == 0) {
			list.Clear();
			return;
		}

		if(count != list.Count) {
			while(list.Count < count) {
				list.Add(default(GameObject));
			}
			if(list.Count > count) {
				list.RemoveRange(count - 1, list.Count - count);
			}
		}

		for(int i = 0; i < count; i++) {
			list[i] = (GameObject)EditorGUILayout.ObjectField(list[i], typeof(GameObject));
		}
	}

	private void DrawInfoSettings(ClutterInfo info) {
		ClutterInfo temp = info;

		EditorGUILayout.LabelField("Number of Objects", EditorStyles.boldLabel);
		temp.count.min = EditorGUILayout.IntField("Min", temp.count.min);
		temp.count.max = EditorGUILayout.IntField("Max", temp.count.max);
		
		EditorGUILayout.LabelField("Distance From Large Object", EditorStyles.boldLabel);
		temp.distance.min = EditorGUILayout.FloatField("Min", temp.distance.min);
		temp.distance.max = EditorGUILayout.FloatField("Max", temp.distance.max);
		
		EditorGUILayout.LabelField("Scale Range", EditorStyles.boldLabel);
		temp.scale.min = EditorGUILayout.FloatField("Min", temp.scale.min);
		temp.scale.max = EditorGUILayout.FloatField("Max", temp.scale.max);
		
		EditorGUILayout.LabelField("X Rotation Range", EditorStyles.boldLabel);
		temp.xRot.min = Mathf.Clamp((EditorGUILayout.FloatField("Min", temp.xRot.min)), -360, 360);
		temp.xRot.max = Mathf.Clamp((EditorGUILayout.FloatField("Max", temp.xRot.max)), -360, 360);
		
		EditorGUILayout.LabelField("Y Rotation Range", EditorStyles.boldLabel);
		temp.yRot.min = Mathf.Clamp((EditorGUILayout.FloatField("Min", temp.yRot.min)), -360, 360);
		temp.yRot.max = Mathf.Clamp((EditorGUILayout.FloatField("Max", temp.yRot.max)), -360, 360);

		EditorGUILayout.LabelField("Z Rotation Range", EditorStyles.boldLabel);
		temp.zRot.min = Mathf.Clamp((EditorGUILayout.FloatField("Min", temp.zRot.min)), -360, 360);
		temp.zRot.max = Mathf.Clamp((EditorGUILayout.FloatField("Max", temp.zRot.max)), -360, 360);

		info = temp;
	}



	private void PlaceClutter(){
		output.Clear();

		GameObject go_0 = new GameObject("Medium Objects");
		go_0.transform.parent = Selection.activeGameObject.transform;
		GetRadialPosition(med, obj.transform.position, medObjs, go_0.transform);

		GameObject go_1 = new GameObject("Small Objects");
		go_1.transform.parent = Selection.activeGameObject.transform;
		GetRadialPosition(small, obj.transform.position, smallObjs, go_1.transform);

		RelaxConstraints();
	}

	private void GetRadialPosition(ClutterInfo info, Vector3 position, List<GameObject> objects, Transform parent) {
		int count = med.count.Rand;
		float slice = 2 * Mathf.PI / count * Random.Range(0.8f, 1.2f);

		for(int i = 0;  i < count; i++) {
			float angle = slice * i;
			GameObject go = UnityEditor.PrefabUtility.InstantiatePrefab(objects[Random.Range(0, objects.Count)]) as GameObject;
			output.Add(go);
			go.transform.SetParent(parent, false);
			go.transform.position = new Vector3(position.x + info.distance.Rand * Mathf.Cos(angle), position.y, position.z + info.distance.Rand * Mathf.Sin(angle));
			go.transform.localScale *=info.scale.Rand;
			go.transform.transform.localRotation = Quaternion.Euler(info.xRot.Rand, info.yRot.Rand, info.zRot.Rand);
		}
	}

	private void RelaxConstraints() {
		float minDist = 5;
		float maxDist = 7;

		Debug.Log(maxDist + " " + minDist);

		foreach (GameObject o1 in output) {
			foreach(GameObject o2 in output) {
				if(o1 != o2){
					RelaxRange(o1.transform, o2.transform, minDist, maxDist, 0.5f, 0.5f);
				}
				RelaxRange(o1.transform, Selection.activeGameObject.transform, minDist, maxDist, 1.0f, 0.0f);
			}
		}
	}

	private void RelaxRange(Transform tpos1, Transform tpos2, float minDist, float maxDist, float comp1, float comp2) {

		Vector3 pos1 = tpos1.position;
		Vector3 pos2 = tpos2.position;
		Vector3 delta = pos2 - pos1;

		float deltaLength = Mathf.Sqrt(delta.x * delta.x + delta.z * delta.z);
		Debug.Log(deltaLength);

		if(deltaLength > 0) {
			float diff = 0.0f;

			if(deltaLength < minDist) {
				diff = (deltaLength - minDist) / deltaLength;
			}
			else if(deltaLength > maxDist) {
				diff = (deltaLength - maxDist) / deltaLength;
			}
			pos1 += delta * comp1 * diff;
			pos2 -= delta * comp2 * diff;
		}

		tpos1.position = pos1;
		tpos2.position = pos2;
	}
}
