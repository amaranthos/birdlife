﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(Waypoint))]
public class WaypointEditor : Editor {
	void OnSceneGUI(){
		var wp = target as Waypoint;
		if(wp == null) return;

		Handles.color = Color.cyan;
		Handles.DrawWireDisc(wp.transform.position + Vector3.up*0.1f, Vector3.up, 2f);

		wp.transform.position = Handles.PositionHandle(wp.transform.position, Quaternion.identity);
	}
}
