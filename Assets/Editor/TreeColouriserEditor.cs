﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(TreeColouriser))]
[CanEditMultipleObjects]
public class TreeColouriserEditor : Editor {
	public override void OnInspectorGUI(){
		DrawDefaultInspector();

		if(targets.Length == 1){
			InspectSingle();
			return;
		}

		var trs = new List<TreeColouriser>();
		bool lockColor = true;

		foreach(var tr in targets){
			var trcol = tr as TreeColouriser;
			if(!trcol) continue;

			trs.Add(trcol);
			lockColor &= trcol.lockColor;
		}

		if(!lockColor && GUILayout.Button("Rerandomise Colour")){
			foreach(var trcol in trs){
				if(!trcol.lockColor)
					trcol.ReRandomiseColour();
			}
		}

		if(GUILayout.Button("Reapply Materials")){
			foreach(var trcol in trs){
				trcol.ApplyMaterials();
			}
		}
	}

	void InspectSingle(){
		var trcol = target as TreeColouriser;
		if(!trcol) return;

		if(!trcol.lockColor && GUILayout.Button("Rerandomise Colour")){
			trcol.ReRandomiseColour();
		}

		if(GUILayout.Button("Reapply Materials")){
			trcol.ApplyMaterials();
		}
	}
}
