﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(FoodSpawner))]
public class FoodSpawnerEditor : Editor {
	Vector3 pos = Vector3.zero;
	FoodSpawner fs;

	public override void OnInspectorGUI(){
		DrawDefaultInspector();
	}

	void OnSceneGUI(){
		int controlId = GUIUtility.GetControlID(FocusType.Passive);
		var e = Event.current;

		fs = target as FoodSpawner;
		if(fs == null) return;
		if(fs.spawnAreas == null) fs.spawnAreas = new List<SpawnArea>();

		switch(e.type){
			case EventType.MouseDown:
				if(e.button != 0) break;

				if(e.shift){
					AddSpawnArea(pos);
					e.Use();
					GUIUtility.hotControl = controlId;
				}else if(e.control){
					DeleteSpawnArea(pos);
					e.Use();
					GUIUtility.hotControl = controlId;
				}

				break;

			case EventType.MouseMove:
				Vector2 mousePos = e.mousePosition;

				var ray = HandleUtility.GUIPointToWorldRay(mousePos);
				RaycastHit hit;
				if(Physics.Raycast(ray, out hit, 1000f)){
					pos = hit.point;
				}

				break;

			case EventType.MouseUp:
				if(e.button != 0) break;

				if(GUIUtility.hotControl == controlId){
					GUIUtility.hotControl = 0;
					e.Use();
				}

				break;

			default: break;
		}

		ProjectSpawnArea(pos);
		DrawSpawnAreas();
		HandleUtility.Repaint();
	}

	void AddSpawnArea(Vector3 position){
		var sa = new SpawnArea(position, 1f);
		fs.spawnAreas.Add(sa);
	}

	void DeleteSpawnArea(Vector3 position){
		fs.spawnAreas.RemoveAll(sa => Vector3.Distance(sa.position, position) < sa.radius);
	}

	void ProjectSpawnArea(Vector3 position){
		float hs = 0.5f;

		if(Event.current.shift){
			Handles.color = Color.green;
		}else if(Event.current.control){
			Handles.color = Color.red;
		}else{
			Handles.color = Color.blue;
		}

		Handles.DrawWireDisc(position + Vector3.up*0.1f, Vector3.up, hs);
	}

	void DrawSpawnAreas(){
		if(fs.spawnAreas.Count == 0) return;

		Vector3[] points = new Vector3[fs.spawnAreas.Count + 1];
		uint i = 0;

		Handles.color = new Color(0.1f, 0.6f, 0.7f, 0.4f);
		foreach(var w in fs.spawnAreas){
			var p = w.position + Vector3.up*0.0f;

			Handles.DrawSolidDisc(p, Vector3.up, w.radius);
			w.position = Handles.PositionHandle(w.position, Quaternion.identity);
			w.radius = Handles.ScaleSlider(w.radius, w.position, -Vector3.right, Quaternion.identity, HandleUtility.GetHandleSize(w.position), 0f);

			points[i++] = p;
		}

		points[i++] = points[0];

		Handles.color = Color.cyan;
	}
}
