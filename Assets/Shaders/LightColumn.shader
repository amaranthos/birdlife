﻿Shader "Custom/LightColumn" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_Softness ("Softness", Float) = 2.0
		_FadeoutThress ("Fadeout Pos", Range(0, 1)) = 0.1
	}

	SubShader {
		Tags {"Queue" = "Transparent" "ForceNoShadowCasting" = "True" }
		Pass{
			Lighting Off Blend SrcAlpha OneMinusSrcAlpha
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"

			struct v2f {
				float4 pos : SV_POSITION;
				float3 normal : NORMAL0;
				float3 viewDir : TEXCOORD0;
				float height : TEXCOORD1;
			};

			v2f vert(appdata_base v) {
				v2f o;
				o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
				o.normal = normalize( mul(float4(v.normal, 0.0), _World2Object).xyz);
				o.height = v.texcoord.y;
				o.viewDir = normalize(WorldSpaceViewDir(v.vertex));
				return o;
			}

			fixed4 _Color;
			float _Softness;
			float _FadeoutThress;

			fixed4 frag(v2f i) : SV_Target {
				float ndv = saturate(dot(i.normal, i.viewDir));
				ndv = pow(ndv, _Softness);

				float fade = 0.0;
				if(i.height > _FadeoutThress){
					fade = (1.0 - i.height)/(1.0 - _FadeoutThress);
				}else{
					fade = i.height/_FadeoutThress;
				}

				fixed4 c = _Color;
				c.a *= fade*ndv;

				return c;
			}

			ENDCG
			
		}
	} 

	FallBack "Diffuse"
}
