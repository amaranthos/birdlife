﻿Shader "Custom/Postprocess" {
	Properties {
		_MainTex ("", 2D) = "white" {}
		vignetteAmt ("Vignette Amount", Float) = 0.0
		vignetteColor("Vignette Colour", Color) = (0,0,0,1)
		desaturateAmt ("Desaturation", Float) = 0.0
		blackOut ("Black Out", Int) = 0
		whiteOutAmt ("White Out Amount", Range(0.0, 1.0)) = 0.0
	}

	SubShader {
		ZTest Always Cull Off ZWrite Off Fog { Mode Off } //Rendering settings

		Pass{
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"

				struct v2f {
					float4 pos : POSITION;
					half2 uv : TEXCOORD0;
				};

				v2f vert(appdata_img v){
					v2f o;
					o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
					o.uv = MultiplyUV(UNITY_MATRIX_TEXTURE0, v.texcoord.xy);
					return o;
				}

				sampler2D _MainTex;
				fixed4 vignetteColor;
				float vignetteAmt;
				float desaturateAmt;
				float whiteOutAmt;
				bool blackOut;

				fixed4 vignette(fixed4 col, half2 uv, float amt){
					float dist = length(uv*2.0 - 1.0);
					dist *= dist;
					// return col/(dist*amt + 1.0);

					return lerp(col, vignetteColor, dist*amt * vignetteColor.a);
				}

				fixed4 grayscale(fixed4 col, float amt){
					float v = (col.r + col.g + col.b)/3.0;
					fixed4 grey = fixed4(v,v,v, 1.0);

					return lerp(col, grey, amt);
				}

				fixed4 whiteOut(fixed4 col, float amt){
					return lerp(col, fixed4(1,1,1,1.0), amt);
				}

				fixed4 frag(v2f i) : COLOR {
					if(blackOut) return fixed4(0,0,0,1);

					fixed4 color = tex2D(_MainTex, i.uv);
					half2 flipuv = i.uv;
					flipuv.y = 1.0 - flipuv.y;

					color = grayscale(color, desaturateAmt);
					color = whiteOut(color, whiteOutAmt);
					color = vignette(color, i.uv, vignetteAmt);

					return color;
				}
			ENDCG
		}	
	} 
	FallBack "Diffuse"
}
